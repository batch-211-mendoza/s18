// Functions
	//Parameters and Arguments
	function printInput(){
		let nickname = prompt("Enter your nickname: ");
		console.log("Hi," + nickname);
	}
	//printInput();

	//However, for other cases, functions can also process data directly passed into it instead of relying only on global variables and prompt()
	function printName(name){
		console.log("My name is " + name);
	}
	printName("Juana");

	//you can directly pass data into the function
	//the function can then call/use that data which is referred as "name" within the function
	//"name" is called a parameter
	//a "parameter" acts as a named variable/container that exists only inside of a function
	//it is used to store information that is provided to a function when it is called or invoked

	//"Juana", the information/data provided directly into a function is called an argument

	//in the following examples, "John" and "Jane" are both arguments since both of them are supplied as information that will be used to print out the full message
	
	printName("John");
	printName("Jane");

	let sampleVariable = "Yui";
	printName(sampleVariable);

	//Function arguments cannot be used by a function if there are no parameters provided within the function

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}
	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);

	function checkDivisibilityBy4(num){
		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?");
		console.log(isDivisibleBy4);
	}
	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);

	//you can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations

	//Functions as Arguments
	//Function parameters can also accept other functions as arguments
	//Some complex functions use other functions as arguments to perform more complicated results
	//This will be further seen when we discuss array methods

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	}

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	//adding and removing the parentheses "()" impacts the output of JavaScript heavily
	//when a function is used with parentheses "()", it denotes invoking/calling a function

	invokeFunction(argumentFunction);
	//a function used without a parenthesis is normally associated with using the function as an argument to another function

	console.log(argumentFunction);
	//or finding more information about a function in the console using console.log()

	//Using multiple parameters
	//mutiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order
	function createFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	}
	createFullName("Juan", "Dela", "Cruz");

	//in JS, providing more/less arguments than the exprected parameters will NOT return an error
	createFullName("Juan", "Bautista");
	createFullName("Jane", "Oliver", "Smith", "Miller");

	let firstName = "Johnny";
	let middleName = "Brown";
	let lastName = "Blaze";

	createFullName(firstName, middleName, lastName);

	function printFullName(middleName, firstName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	}
	printFullName("Juan", "Dela", "Cruz"); //output: "Dela Juan Cruz"
	//because "Juan" was received as middleName, "Dela" was received as firstName, and "Cruz" was received as lastName

	/*
		Mini-Activity
		create a function called printFriends with the ff:
		• 3 parameters
		• message: "My three friends are: friend1, friend2, and friend3"
	*/

	function printFriends(friend1, friend2, friend3){
		console.log("My three friends are: "+ friend1 + ", " + friend2 + ", and " + friend3);
	}
	printFriends("Ed", "Edd", "Eddy");

	//The Return Statement

	//The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
	//the "returnFullName" function was called in the same line as declaring a variable

	//whatever value is returned from "returnFullName" function is stored in the "completeName" variable

	function returnFullName(firstName, middleName, lastName){
		// console.log(firstName + " " + middleName + " " + lastName);
		return firstName + " " + middleName + " " + lastName;
		console.log("This message will not be printed."); //notice that the console.log after the return statement is no longer printed in the console. That is because ideally, any line/code that comes after the return statement is ignored because it ends the function execution
	}
	let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completeName);

	//this way, a function is able to returna  value we can further use or manipulate in our program instead of only printing/displaying it in the console

	console.log(returnFullName(firstName, middleName, lastName));
	//in this example, console.log() will print the returned value of the returnFullName() function

	function returnAddress(city, country){
		let fullAddress = city + ", " + country;
		return fullAddress;
	}
	let myAddress = returnAddress("Cebu City", "Philippines");
	console.log(myAddress);

	function printPlayerInfo(username, level, job){
		// console.log("Username: " + username);
		// console.log("Level: " + level);
		// console.log("Job: " + job);
		return("Username: " + username + " Level: " + level + " Job: " + job)
	}
	let user1 = printPlayerInfo("knight_white", 95, "Paladin");
	console.log(user1); //undefined

	/*
		Mini-Activity:
		Create a function that will be able to multiply two numbers
		-Numbers must be provided as arguments
		-Return the result of the multiplication

		Create a global variable outside of the function called "product"
			-this product variable should be able to receive and store the result of the multiplication function

		Log the value of the product variable in the console
	*/

	function returnProduct(num1, num2){
		let product = num1 * num2;
		return ("The product of " + num1 + " and " + num2 + " is : " + product);
	}
	let product = returnProduct(8, 10);
	console.log(product);

	//Instructor's Solution
	function multiplyNumbers(num1, num2){
		return num1*num2;
	}
	let product = multiplyNumbers(5, 4);
	console.log("The product of 5 and 4 is: ");
	console.log(product);